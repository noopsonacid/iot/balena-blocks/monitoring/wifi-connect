#!/bin/bash

if [[ ! -z $CHECK_CONN_FREQ ]] 
    then
        freq=$CHECK_CONN_FREQ
    else
        freq=120
fi

# Optional step - it takes couple of seconds (or longer) to establish a WiFi connection
# sometimes. In this case, following checks will fail and wifi-connect
# will be launched even if the device will be able to connect to a WiFi network.
# If this is your case, you can wait for a while and then check for the connection.

sleep 5

while [[ true ]]; do
    # Choose a condition for running WiFi Connect according to your use case:

    # echo "Is there a default gateway?"
    # ip route | grep default

    # echo "Is there Internet connectivity?"
    # nmcli -t g | grep full

    # echo "Is there Internet connectivity via a google ping?"
    # wget --spider http://google.com 2>&1

    echo "Is there an active WiFi connection?"
    iwgetid -r

    if [ $? -eq 0 ]; then
        echo "Skipping setting up Wifi-Connect Access Point. Will check again in $freq seconds"
    else
        echo "Active WiFi connection is not found.\n"
        echo "Starting up Wifi-Connect.\n"
        echo "Connect to the Access Point and configure the SSID and Passphrase for the network to connect to."
        DBUS_SYSTEM_BUS_ADDRESS=unix:path=/host/run/dbus/system_bus_socket /usr/src/app/wifi-connect -u /usr/src/app/ui
    fi

    sleep $freq

done

# Start your application here.
/usr/bin/balena-idle
